/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab.d3if3803.jurnal.model;

/**
 *
 * @author Agi Maulana
 */
public class Buku {
    public static final String KODE = "Kode";
    public static final String JUDUL = "Judul";
    public static final String PENERBIT = "Penerbit";
    public static final String HARGA = "Harga";
    public static final String AVAILABLE = "Available";
    
    private String kode;
    private String judul;
    private String penerbit;
    private long harga;
    
    public Buku(){}

    public Buku(String kode, String judul, String penerbit, long harga) {
        this.kode = kode;
        this.judul = judul;
        this.penerbit = penerbit;
        this.harga = harga;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }
       
}
