/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab.d3if3803.jurnal.model;

import config.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Agi Maulana
 */
public class DBAdapter {
    public ArrayList<Buku> getBuku(){
        ArrayList<Buku> lsBuku = new ArrayList<Buku>();
        try{
            String query = "select * from buku";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            lsBuku = fetchBuku(rs);
//            while(rs.next()){
//                Buku buku = new Buku();
//                buku.setKode(rs.getString(Buku.KODE));
//                buku.setJudul(rs.getString(Buku.JUDUL));
//                buku.setPenerbit(rs.getString(Buku.PENERBIT));
//                buku.setHarga(rs.getInt(Buku.HARGA));
//                
//                lsBuku.add(buku);
//            }
            db.close();
        }catch(SQLException e){
            System.err.println("Error getBuku() : " + e.getMessage());
        }
        
        return lsBuku;
    }
    
    public ArrayList<Buku> getBuku(String keyword, int columnIndex){
    ArrayList<Buku> lsBuku = new ArrayList<Buku>();
        try{
            String query = "";
            switch(columnIndex){
                case 0:
                    query = "select * from buku where "
                    + "kode = '"+ keyword +"' || "
                    + "judul like '%"+ keyword +"%' || "
                    + "penerbit like '%"+ keyword +"%' || "
                    + "harga = '"+ keyword +"'";
                    break;
                case 1:
                    query = "select * from buku where kode='"+ keyword +"'";
                    break;
                case 2:
                    query = "select * from buku where judul like '%"+ keyword +"%'";
                    break;
                case 3:
                    query = "select * from buku where penerbit like '%"+ keyword +"%'";
                    break;
                case 4:
                    query = "select * from buku where harga='"+ keyword +"'";
                    break;
                default:
                    return lsBuku;
            }
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            lsBuku = fetchBuku(rs);
            db.close();
        }catch(SQLException e){
            System.err.println("Error getBuku(String keyword, int columnIndex) : " + e.getMessage());
        }
        
        return lsBuku;
    }
    
    public ArrayList<Buku> fetchBuku(ResultSet rs) throws SQLException{
        ArrayList<Buku> lsBuku = new ArrayList<Buku>();
        while(rs.next()){
            Buku buku = new Buku();
            buku.setKode(rs.getString(Buku.KODE));
            buku.setJudul(rs.getString(Buku.JUDUL));
            buku.setPenerbit(rs.getString(Buku.PENERBIT));
            buku.setHarga(rs.getInt(Buku.HARGA));
                
            lsBuku.add(buku);
        }
        
        return lsBuku;
    }
    
    public int deleteBuku(String kode){
        int result = -1;
        try{
            String query = "delete from buku where kode = '"+ kode +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            result = stt.executeUpdate(query);
            db.close();
        }catch(SQLException e){
            System.err.println("Error deleteBuku(String kode) : " + e.getMessage());
        }
        
        return result;
    }
    
    public int updateBuku(Buku buku){
        int result = -1;
        try{
            String query = "update buku set "
                    + "judul ='"+ buku.getJudul() +"', "
                    + "penerbit ='"+ buku.getPenerbit() +"', "
                    + "harga ='"+ buku.getHarga() +"' "
                    + "where kode = '"+ buku.getKode() +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            result = stt.executeUpdate(query);
            db.close();
        }catch(SQLException e){
            System.err.println("Error updateBuku(Buku buku) : " + e.getMessage());
        }
        
        return result;
    }
    
    public int tambahBuku(Buku buku){
        int result = -1;
        try{
            String query = "insert into buku values("
                    +"'"+ buku.getKode() +"',"
                    + "'"+ buku.getJudul() +"',"
                    + "'"+ buku.getPenerbit()+"',"
                    + "'"+ buku.getHarga() +"',"
                    +"'"+ 1 +"')";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            result = stt.executeUpdate(query);
            db.close();
        }catch(SQLException e){
            System.err.println("Error tambahBuku(Buku buku) : " + e.getMessage());
        }
        
        return result;
    }
    
    public boolean bukuIsExists(String kode){
        try{
            String query = "select count(*) as total from buku "
                    + "where kode ='"+ kode +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            while(rs.next())
                if(rs.getInt("total") > 0)
                    return true;
            db.close();
        }catch(SQLException e){
            System.err.println("Error bukuIsExists(String kode) : " + e.getMessage());
        }
        
        return false;
    }
    
    public boolean pinjam(String kodeBuku, int lamaHari){
        try{
            String query = "update buku set available = '0' "
                    + "where kode = '"+ kodeBuku +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            if(stt.executeUpdate(query) > 0)
                return true;
            db.close();
        }catch(SQLException e){
            System.err.println("Error pinjam(String kodeBuku, int lamaHari) : " + e.getMessage());
        }
        
        return false;
    }
    
    public boolean kembali(String kodeBuku){
        try{
            String query = "update buku set available = '1' "
                    + "where kode = '"+ kodeBuku +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            if(stt.executeUpdate(query) > 0)
                return true;
            db.close();
        }catch(SQLException e){
            System.err.println("Error kembali(String kodeBuku) : " + e.getMessage());
        }
        
        return false;
    }
    
    public boolean isAvailable(String kodeBuku){
        try{
            String query = "select available from buku where kode ='"+ kodeBuku +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            while(rs.next())
                if(rs.getInt("available") == 1)
                    return true;
            db.close();
        }catch(SQLException e){
            System.err.println("Error isAvailable(String kodeBuku) : " + e.getMessage());
        }
        
        return false;
    }
    
    public int getHarga(String kodeBuku){
        int harga = -1;
        try{
            String query = "select harga from buku where kode='"+ kodeBuku +"'";
            Database db = new Database();
            db.open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            while(rs.next())
                harga = rs.getInt(Buku.HARGA);
            db.close();
        }catch(SQLException e){
            System.err.println("Error getHarga(String kodeBuku) : " + e.getMessage());
        }
        
        return harga;
    }
}
