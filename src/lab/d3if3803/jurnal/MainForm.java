/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab.d3if3803.jurnal;

import com.mysql.jdbc.StringUtils;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import lab.d3if3803.jurnal.model.Buku;
import lab.d3if3803.jurnal.model.DBAdapter;

/**
 *
 * @author Agi Maulana
 */
public class MainForm extends javax.swing.JFrame {

    /**
     * Creates new form MainForm
     */
    
    private DBAdapter dbAdapter = new DBAdapter();
    public MainForm() {
        initComponents();
        initTableDataBuku();
        
        setLocation(300, 30);
    }
    
    private void initTableDataBuku(){
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn(Buku.KODE);
        tableModel.addColumn(Buku.JUDUL);
        tableModel.addColumn(Buku.PENERBIT);
        tableModel.addColumn(Buku.HARGA);
        
        ArrayList<Buku> lsBuku = dbAdapter.getBuku();
        if(lsBuku.size() > 0){
            for(int i = 0; i < lsBuku.size(); i++){
                String[] arrBuku = new String[4];
                arrBuku[0] = lsBuku.get(i).getKode();
                arrBuku[1] = lsBuku.get(i).getJudul();
                arrBuku[2] = lsBuku.get(i).getPenerbit();
                arrBuku[3] = String.valueOf(lsBuku.get(i).getHarga());

                tableModel.addRow(arrBuku);
            }
        }
        
        tableDataBuku.setModel(tableModel);
    }
    
    private void cariBuku(){
        if(txtDataBukuCari.getText().isEmpty()){
            showErrorMessage("Masukkan kata kunci");
            return;
        }
        
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn(Buku.KODE);
        tableModel.addColumn(Buku.JUDUL);
        tableModel.addColumn(Buku.PENERBIT);
        tableModel.addColumn(Buku.HARGA);
        
        int columnPosition = comboDataBukuCari.getSelectedIndex();
        String keyword = txtDataBukuCari.getText();
        ArrayList<Buku> lsBuku = dbAdapter.getBuku(keyword, columnPosition);
        if(lsBuku.size() > 0){
            for(int i = 0; i < lsBuku.size(); i++){
                String[] arrBuku = new String[4];
                arrBuku[0] = lsBuku.get(i).getKode();
                arrBuku[1] = lsBuku.get(i).getJudul();
                arrBuku[2] = lsBuku.get(i).getPenerbit();
                arrBuku[3] = String.valueOf(lsBuku.get(i).getHarga());

                tableModel.addRow(arrBuku);
            }
        }
        
        tableDataBuku.setModel(tableModel);
    }
    
    private void resetManageBukuForm(){
        txtManageBukuKode.setText("");
        txtManageBukuJudul.setText("");
        txtManageBukuPenerbit.setText("");
        txtManageBukuHarga.setText("");
    }
    
    private void deleteBuku(){
        if(!dbAdapter.bukuIsExists(txtManageBukuKode.getText())){
            showErrorMessage("Buku dengan kode " + txtManageBukuKode.getText()
                    +" tidak ditemukan");
            return;
        }
        
        if(dbAdapter.deleteBuku(txtManageBukuKode.getText()) > 0)
            showSuccessMessage("Buku berhasil dihapus");
        
        resetManageBukuForm();
        initTableDataBuku();
    }
    
    public void updateBuku(){
        String kode = txtManageBukuKode.getText();
        String judul = txtManageBukuJudul.getText();
        String penerbit = txtManageBukuPenerbit.getText();
        String harga = txtManageBukuHarga.getText();
        
        if(kode.isEmpty() || judul.isEmpty() || penerbit.isEmpty() || harga.isEmpty()){
            showErrorMessage("Lengkapi kolom manage buku");
            return;
        }
        
        if(!dbAdapter.bukuIsExists(txtManageBukuKode.getText())){
            showErrorMessage("Buku dengan kode " + txtManageBukuKode.getText()
                    +" tidak ditemukan");
            return;
        }
        
        if(!isNumber(harga)){
            showErrorMessage("Hanya masukkan angka pada kolom harga");
            return;
        }
        
        Buku buku = new Buku(kode, judul, penerbit, Integer.parseInt(harga));
        if(dbAdapter.updateBuku(buku) > 0)
            showSuccessMessage("Buku berhasil di update");
        
        resetManageBukuForm();
        initTableDataBuku();
    }
    
    public void tambahBuku(){
        
        String kode = txtManageBukuKode.getText();
        String judul = txtManageBukuJudul.getText();
        String penerbit = txtManageBukuPenerbit.getText();
        String harga = txtManageBukuHarga.getText();
        
        if(kode.isEmpty() || judul.isEmpty() || penerbit.isEmpty() || harga.isEmpty()){
            showErrorMessage("Lengkapi kolom manage buku");
            return;
        }
        
        if(dbAdapter.isAvailable(kode)){
            showErrorMessage("Buku dengan kode " + kode + " sudah ada");
            return;
        }
        
        if(!isNumber(harga)){
            showErrorMessage("Hanya masukkan angka pada kolom harga");
            return;
        }
        
        Buku buku = new Buku(kode, judul, penerbit, Integer.parseInt(harga));
        if(dbAdapter.tambahBuku(buku) > 0)
            showSuccessMessage("Buku berhasil di tambah");
        
        resetManageBukuForm();
        initTableDataBuku();
    }
    
    private void pinjamBuku(){
        if(txtPeminjamanKodeBuku.getText().isEmpty() || txtPeminjamanLamaHari.getText().isEmpty()){
            showErrorMessage("Lengkapi kode buku dan lama peminjaman");
            return;
        }
        
        if(!dbAdapter.bukuIsExists(txtPeminjamanKodeBuku.getText())){
            showErrorMessage("Buku dengan kode " + txtPeminjamanKodeBuku.getText()
                    +" tidak ditemukan");
            return;
        }
        if(!isNumber(txtPeminjamanLamaHari.getText())){
            showErrorMessage("Hanya masukkan angka pada kolom lama peminjaman ");
            return;
        }
        
        String kode = txtPeminjamanKodeBuku.getText();
        int lamaHari = Integer.parseInt(txtPeminjamanLamaHari.getText());
        if(!dbAdapter.isAvailable(kode)){
            showErrorMessage("Buku dengan kode " + kode + " tidak tersedia");
            return;
        }
        
        if(dbAdapter.pinjam(kode, lamaHari)){
            int totalHarga = dbAdapter.getHarga(kode) * lamaHari;
            showSuccessMessage("Buku berhasil dipinjam. Total harga Rp " + totalHarga);
            txtPeminjamanKodeBuku.setText("");
            txtPeminjamanLamaHari.setText("");
        }
    }
    
    private void kembalikanBuku(){
        if(txtPengembalianKodeBuku.getText().isEmpty()){
            showErrorMessage("Masukkan kode buku");
            return;
        }
        
        if(!dbAdapter.bukuIsExists(txtPengembalianKodeBuku.getText())){
            showErrorMessage("Buku dengan kode " + txtPengembalianKodeBuku.getText()
                    +" tidak ditemukan");
            return;
        }
        
        String kode = txtPengembalianKodeBuku.getText();
        if(dbAdapter.isAvailable(kode)){
            showErrorMessage("Buku dengan kode " + kode + " tidak dalam masa "
                    + "peminjaman");
            return;
        }
        
        if(dbAdapter.kembali(kode)){
            showSuccessMessage("Buku berhasil dikembalikan");
            txtPengembalianKodeBuku.setText("");
        }
    }
    
    private void showErrorMessage(String message){
        JOptionPane.showMessageDialog(rootPane, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    private void showSuccessMessage(String message){
        JOptionPane.showMessageDialog(rootPane, message, "Success", JOptionPane.ERROR_MESSAGE);
    }
    
    private boolean isNumber(String string){
        if(string.matches("^\\d+$")) return true;
        else return false;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtManageBukuKode = new javax.swing.JTextField();
        txtManageBukuJudul = new javax.swing.JTextField();
        txtManageBukuPenerbit = new javax.swing.JTextField();
        txtManageBukuHarga = new javax.swing.JTextField();
        btnManageBukuReset = new javax.swing.JButton();
        btnManageBukuDelete = new javax.swing.JButton();
        btnManageBukuUpdate = new javax.swing.JButton();
        btnManageBukuTambah = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtPeminjamanKodeBuku = new javax.swing.JTextField();
        txtPeminjamanLamaHari = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        btnPeminjamanPinjam = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtPengembalianKodeBuku = new javax.swing.JTextField();
        btnPengembalianUbahStatus = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtDataBukuCari = new javax.swing.JTextField();
        comboDataBukuCari = new javax.swing.JComboBox();
        btnDataBukuCari = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDataBuku = new javax.swing.JTable();
        txtDataBukuRefresh = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 2, true));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Manage Buku");

        jLabel2.setText("Kode");

        jLabel3.setText("Judul");

        jLabel4.setText("Penerbit");

        jLabel5.setText("Harga");

        btnManageBukuReset.setText("Reset");
        btnManageBukuReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageBukuResetActionPerformed(evt);
            }
        });

        btnManageBukuDelete.setText("Delete");
        btnManageBukuDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageBukuDeleteActionPerformed(evt);
            }
        });

        btnManageBukuUpdate.setText("Update");
        btnManageBukuUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageBukuUpdateActionPerformed(evt);
            }
        });

        btnManageBukuTambah.setText("Tambah");
        btnManageBukuTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageBukuTambahActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtManageBukuHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtManageBukuPenerbit, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtManageBukuJudul, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtManageBukuKode, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(btnManageBukuReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnManageBukuUpdate))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnManageBukuTambah)
                                    .addComponent(btnManageBukuDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtManageBukuKode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtManageBukuJudul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtManageBukuPenerbit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtManageBukuHarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnManageBukuReset)
                    .addComponent(btnManageBukuDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnManageBukuUpdate)
                    .addComponent(btnManageBukuTambah))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 2, true));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Peminjaman");

        jLabel7.setText("Kode Buku");

        jLabel8.setText("Lama");

        txtPeminjamanLamaHari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPeminjamanLamaHariActionPerformed(evt);
            }
        });

        jLabel9.setText("Hari");

        btnPeminjamanPinjam.setText("Pinjam");
        btnPeminjamanPinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPeminjamanPinjamActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnPeminjamanPinjam, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel7)
                                .addComponent(jLabel8))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtPeminjamanKodeBuku, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(txtPeminjamanLamaHari, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel9))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(txtPeminjamanKodeBuku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPeminjamanLamaHari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(26, 26, 26)
                .addComponent(btnPeminjamanPinjam)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 2, true));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Pengembalian");

        jLabel11.setText("Kode Buku");

        btnPengembalianUbahStatus.setText("Ubah Status");
        btnPengembalianUbahStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPengembalianUbahStatusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnPengembalianUbahStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                            .addComponent(txtPengembalianKodeBuku))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtPengembalianKodeBuku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPengembalianUbahStatus)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 2, true));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Data Buku");

        comboDataBukuCari.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Semua", "Kode", "Judul", "Penerbit", "Harga" }));

        btnDataBukuCari.setText("Cari");
        btnDataBukuCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDataBukuCariActionPerformed(evt);
            }
        });

        tableDataBuku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableDataBuku);

        txtDataBukuRefresh.setText("Refresh");
        txtDataBukuRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDataBukuRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtDataBukuCari, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboDataBukuCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDataBukuCari))
                            .addComponent(txtDataBukuRefresh))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addGap(2, 2, 2)
                .addComponent(txtDataBukuRefresh)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDataBukuCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboDataBukuCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDataBukuCari))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageBukuDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageBukuDeleteActionPerformed
        // TODO add your handling code here:
        deleteBuku();
    }//GEN-LAST:event_btnManageBukuDeleteActionPerformed

    private void txtPeminjamanLamaHariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPeminjamanLamaHariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPeminjamanLamaHariActionPerformed

    private void btnManageBukuResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageBukuResetActionPerformed
        // TODO add your handling code here:
        resetManageBukuForm();
    }//GEN-LAST:event_btnManageBukuResetActionPerformed

    private void btnManageBukuUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageBukuUpdateActionPerformed
        // TODO add your handling code here:
        updateBuku();
    }//GEN-LAST:event_btnManageBukuUpdateActionPerformed

    private void btnManageBukuTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageBukuTambahActionPerformed
        // TODO add your handling code here:
        tambahBuku();
    }//GEN-LAST:event_btnManageBukuTambahActionPerformed

    private void txtDataBukuRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDataBukuRefreshActionPerformed
        // TODO add your handling code here:
        initTableDataBuku();
    }//GEN-LAST:event_txtDataBukuRefreshActionPerformed

    private void btnPeminjamanPinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPeminjamanPinjamActionPerformed
        // TODO add your handling code here:
        pinjamBuku();
    }//GEN-LAST:event_btnPeminjamanPinjamActionPerformed

    private void btnPengembalianUbahStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPengembalianUbahStatusActionPerformed
        // TODO add your handling code here:
        kembalikanBuku();
    }//GEN-LAST:event_btnPengembalianUbahStatusActionPerformed

    private void btnDataBukuCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDataBukuCariActionPerformed
        // TODO add your handling code here:
        cariBuku();
    }//GEN-LAST:event_btnDataBukuCariActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDataBukuCari;
    private javax.swing.JButton btnManageBukuDelete;
    private javax.swing.JButton btnManageBukuReset;
    private javax.swing.JButton btnManageBukuTambah;
    private javax.swing.JButton btnManageBukuUpdate;
    private javax.swing.JButton btnPeminjamanPinjam;
    private javax.swing.JButton btnPengembalianUbahStatus;
    private javax.swing.JComboBox comboDataBukuCari;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableDataBuku;
    private javax.swing.JTextField txtDataBukuCari;
    private javax.swing.JButton txtDataBukuRefresh;
    private javax.swing.JTextField txtManageBukuHarga;
    private javax.swing.JTextField txtManageBukuJudul;
    private javax.swing.JTextField txtManageBukuKode;
    private javax.swing.JTextField txtManageBukuPenerbit;
    private javax.swing.JTextField txtPeminjamanKodeBuku;
    private javax.swing.JTextField txtPeminjamanLamaHari;
    private javax.swing.JTextField txtPengembalianKodeBuku;
    // End of variables declaration//GEN-END:variables
}
